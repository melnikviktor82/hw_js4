"use strict";

// ## Теоретичні питання

// 1. Описати своїми словами навіщо потрібні функції у програмуванні.

// ФункціЇ потрібні, щоб повторити один код в різних частинах нашоЇ програми, просто визвавши там функцію.

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.

// Щоб передати всередину функції інформацію, з якою вона буде працювати.

// 3. Що таке оператор return та як він працює всередині функції?

// Повертає результат, функція зупиняється і значення повертається в код, що викликав її.

// #### Технічні вимоги:

// - Отримати за допомогою модального вікна браузера два числа.
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено `+`, `-`, `*`, `/`.
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

let number1 = prompt("Enter number#1");

let number2 = prompt("Enter number#2");

function numberCheck(num1, num2) {
  return (
    isNaN(number1) ||
    number1 === null ||
    number1 === "" ||
    isNaN(number2) ||
    number2 === null ||
    number2 === "" ||
    number1 === " " ||
    number2 === " "
  );
}

while (numberCheck(number1, number2)) {
  alert("Error! Arguments are not numbers");
  number1 = prompt("Enter number#1", [number1]);
  number2 = prompt("Enter number#2", [number2]);
}

let operation = prompt("Enter math operation: `+`, `-`, `*`, `/`");
switch (operation) {
  case "+":
    operation = add;
    break;
  case "-":
    operation = subtract;
    break;
  case "*":
    operation = multiply;
    break;
  case "/":
    operation = divide;
    break;
  default:
    alert("Unknown math operation");
}

function add(number1, number2) {
  return +number1 + +number2;
}

function subtract(number1, number2) {
  return number1 - number2;
}

function multiply(number1, number2) {
  return number1 * number2;
}

function divide(number1, number2) {
  return number1 / number2;
}

function calculate(number1, number2, operation) {
  return operation(number1, number2);
}

console.log(calculate(number1, number2, operation));
